Source: rust-blake3
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-arrayref-0.3+default-dev (>= 0.3.5-~~) <!nocheck>,
 librust-arrayvec-0.7-dev <!nocheck>,
 librust-cc-1+default-dev (>= 1.0.4-~~) <!nocheck>,
 librust-cfg-if-1+default-dev <!nocheck>,
 librust-constant-time-eq-0.1+default-dev (>= 0.1.5-~~) <!nocheck>,
 librust-digest-0.10+std-dev (>= 0.10.1-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 karthek <mail@karthek.com>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/blake3]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/blake3
Rules-Requires-Root: no

Package: librust-blake3-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-arrayref-0.3+default-dev (>= 0.3.5-~~),
 librust-arrayvec-0.7-dev,
 librust-cc-1+default-dev (>= 1.0.4-~~),
 librust-cfg-if-1+default-dev,
 librust-constant-time-eq-0.1+default-dev (>= 0.1.5-~~),
 librust-digest-0.10+default-dev (>= 0.10.1-~~),
 librust-digest-0.10+mac-dev (>= 0.10.1-~~),
 librust-digest-0.10+std-dev (>= 0.10.1-~~),
 librust-rayon-1+default-dev (>= 1.1.0-~~)
Provides:
 librust-blake3+default-dev (= ${binary:Version}),
 librust-blake3+digest-dev (= ${binary:Version}),
 librust-blake3+neon-dev (= ${binary:Version}),
 librust-blake3+no-avx2-dev (= ${binary:Version}),
 librust-blake3+no-avx512-dev (= ${binary:Version}),
 librust-blake3+no-neon-dev (= ${binary:Version}),
 librust-blake3+no-sse2-dev (= ${binary:Version}),
 librust-blake3+no-sse41-dev (= ${binary:Version}),
 librust-blake3+prefer-intrinsics-dev (= ${binary:Version}),
 librust-blake3+pure-dev (= ${binary:Version}),
 librust-blake3+rayon-dev (= ${binary:Version}),
 librust-blake3+std-dev (= ${binary:Version}),
 librust-blake3+traits-preview-dev (= ${binary:Version}),
 librust-blake3-1-dev (= ${binary:Version}),
 librust-blake3-1+default-dev (= ${binary:Version}),
 librust-blake3-1+digest-dev (= ${binary:Version}),
 librust-blake3-1+neon-dev (= ${binary:Version}),
 librust-blake3-1+no-avx2-dev (= ${binary:Version}),
 librust-blake3-1+no-avx512-dev (= ${binary:Version}),
 librust-blake3-1+no-neon-dev (= ${binary:Version}),
 librust-blake3-1+no-sse2-dev (= ${binary:Version}),
 librust-blake3-1+no-sse41-dev (= ${binary:Version}),
 librust-blake3-1+prefer-intrinsics-dev (= ${binary:Version}),
 librust-blake3-1+pure-dev (= ${binary:Version}),
 librust-blake3-1+rayon-dev (= ${binary:Version}),
 librust-blake3-1+std-dev (= ${binary:Version}),
 librust-blake3-1+traits-preview-dev (= ${binary:Version}),
 librust-blake3-1.3-dev (= ${binary:Version}),
 librust-blake3-1.3+default-dev (= ${binary:Version}),
 librust-blake3-1.3+digest-dev (= ${binary:Version}),
 librust-blake3-1.3+neon-dev (= ${binary:Version}),
 librust-blake3-1.3+no-avx2-dev (= ${binary:Version}),
 librust-blake3-1.3+no-avx512-dev (= ${binary:Version}),
 librust-blake3-1.3+no-neon-dev (= ${binary:Version}),
 librust-blake3-1.3+no-sse2-dev (= ${binary:Version}),
 librust-blake3-1.3+no-sse41-dev (= ${binary:Version}),
 librust-blake3-1.3+prefer-intrinsics-dev (= ${binary:Version}),
 librust-blake3-1.3+pure-dev (= ${binary:Version}),
 librust-blake3-1.3+rayon-dev (= ${binary:Version}),
 librust-blake3-1.3+std-dev (= ${binary:Version}),
 librust-blake3-1.3+traits-preview-dev (= ${binary:Version}),
 librust-blake3-1.3.1-dev (= ${binary:Version}),
 librust-blake3-1.3.1+default-dev (= ${binary:Version}),
 librust-blake3-1.3.1+digest-dev (= ${binary:Version}),
 librust-blake3-1.3.1+neon-dev (= ${binary:Version}),
 librust-blake3-1.3.1+no-avx2-dev (= ${binary:Version}),
 librust-blake3-1.3.1+no-avx512-dev (= ${binary:Version}),
 librust-blake3-1.3.1+no-neon-dev (= ${binary:Version}),
 librust-blake3-1.3.1+no-sse2-dev (= ${binary:Version}),
 librust-blake3-1.3.1+no-sse41-dev (= ${binary:Version}),
 librust-blake3-1.3.1+prefer-intrinsics-dev (= ${binary:Version}),
 librust-blake3-1.3.1+pure-dev (= ${binary:Version}),
 librust-blake3-1.3.1+rayon-dev (= ${binary:Version}),
 librust-blake3-1.3.1+std-dev (= ${binary:Version}),
 librust-blake3-1.3.1+traits-preview-dev (= ${binary:Version})
Description: BLAKE3 hash function - Rust source code
 This package contains the source for the Rust blake3 crate, packaged by
 debcargo for use with cargo and dh-cargo.
